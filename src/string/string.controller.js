const { isString } = require("../utils/is-string");
const { HttpError } = require("../errors/http-error");

class StringController {
  reverse(req, res) {
    const text = this.validate(req.body),
      reversed = text.split("").reverse().join("");

    res.header("Content-Type", "text/plain").send(reversed);
  }

  validate(body) {
    if (!isString(body)) {
      throw new HttpError("Request body should be a string", 422);
    }

    return body;
  }
}

module.exports = { stringController: new StringController() };
