const isString = (item) => typeof item === "string";

module.exports = { isString };
