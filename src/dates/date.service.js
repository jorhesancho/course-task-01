const { weekDayIndex } = require("./constants/week-day.index");
const { INVALID_DATE_STRING } = require("./constants/invalid-date-string");
const { MS_IN_DAY } = require("./constants/time-constants");

class DateService {
  createDate(year, month, day) {
    return new Date(`${year}-${month}-${day}`);
  }

  isValidDate(year, month, day) {
    return this.createDate(year, month, day).toString() !== INVALID_DATE_STRING;
  }

  getWeekDay(dayIndex) {
    return weekDayIndex[dayIndex];
  }

  isLeapYear(year) {
    return (year % 4 === 0 && year % 100 !== 0) || year % 400 === 0;
  }

  diffDates(date1, date2) {
    return (this.toUtc(date2) - this.toUtc(date1)) / MS_IN_DAY;
  }

  toUtc(date) {
    return Date.UTC(date.getFullYear(), date.getMonth(), date.getDate());
  }
}

module.exports = { dateService: new DateService() };
