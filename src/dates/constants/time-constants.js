const MS_IN_SEC = 1000;

const SEC_IN_MIN = 60;

const MIN_IN_HR = 60;

const HR_IN_DAY = 24;

const MS_IN_DAY = MS_IN_SEC * SEC_IN_MIN * MIN_IN_HR * HR_IN_DAY;

module.exports = {
  MS_IN_SEC,
  SEC_IN_MIN,
  MIN_IN_HR,
  HR_IN_DAY,
  MS_IN_DAY,
};
