const { HttpError } = require("../errors/http-error");
const { dateService } = require("./date.service");
const { isString } = require("../utils/is-string");

class DateController {
  getDateInfo(req, res) {
    const date = this.validate(req.params);

    res.json({
      weekDay: dateService.getWeekDay(date.getDay()),
      isLeapYear: dateService.isLeapYear(date.getFullYear()),
      difference: Math.abs(dateService.diffDates(date, new Date())),
    });
  }

  validate(params) {
    const { year, month, day } = params;

    if (
      ![year, month, day].every(isString) ||
      !dateService.isValidDate(year, month, day)
    ) {
      throw new HttpError("Invalid date format", 422);
    }

    return dateService.createDate(year, month, day);
  }
}

module.exports = { dateController: new DateController() };
