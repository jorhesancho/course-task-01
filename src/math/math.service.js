class MathService {
  square(number) {
    return Math.pow(number, 2);
  }
}

module.exports = { mathService: new MathService() };
