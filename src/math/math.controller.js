const { HttpError } = require("../errors/http-error");
const { mathService } = require("./math.service");

class MathController {
  square(req, res) {
    const number = this.validate(req.body),
      squared = mathService.square(number);

    res.json({
      number,
      square: squared,
    });
  }

  validate(body) {
    const number = parseFloat(body);

    if (isNaN(number)) {
      throw new HttpError("The body should be a number", 400);
    }

    return number;
  }
}

module.exports = { mathController: new MathController() };
