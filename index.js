require("dotenv").config();
const express = require("express");

const { errorMiddleware } = require("./src/errors/error-middleware");

const { mathController } = require("./src/math/math.controller");
const { dateController } = require("./src/dates/date.controller");
const { stringController } = require("./src/string/string.controller");

const app = express();

app.post("/square", express.text(), (req, res, next) =>
  mathController.square(req, res, next)
);

app.post("/reverse", express.text(), (req, res, next) =>
  stringController.reverse(req, res, next)
);

app.get("/date/:year/:month/:day", (req, res, next) =>
  dateController.getDateInfo(req, res, next)
);

app.use(errorMiddleware);

const PORT = parseInt(process.env.PORT) || 56201;

app.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`);
});
